//Variables
const form = document.querySelector('#formulario');
const error  = document.querySelector("#error1")


//Click en el boton ingresar
form.addEventListener("submit", singIn);


//Funciones 
function singIn (e) {
    e.preventDefault();
    
    var data = new FormData(form);
    var email = data.get('email');
    var pass = data.get('pass');
    
    
    
    fetch('php/singin.php',{
        method:'POST',
        body: data
    })
    .then(res => res.text())
    .then(res =>{
        
        /******Aca devuelve la respuesta******/
         if(res === 'Datos Incorrectos' || res==='Email Incorrecto' || res === 'Contraseña Incorrecta'){
            msjError(res);
            }else{
            window.location= 'dash.html';
        } 
    });
}
function msjError(answ){
    const cardError = document.createElement('div');
    cardError.textContent = answ;
    cardError.classList.add('alert', 'alert-danger');
    error.appendChild(cardError);
    setTimeout(function(){  cardError.remove(cardError);; }, 3000);
}

