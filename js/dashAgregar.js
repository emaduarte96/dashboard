//Variables
const formDash = document.querySelector('#formularioDash');
const cardAviso= document.querySelector('#cardAviso');


//Eventos
formDash.addEventListener("submit", agregarProducto);

//funciones
function agregarProducto(e){
    e.preventDefault();

    const data = new FormData(formDash);
    const index = data.get('id');
    const nombre = data.get('nombre');
    const descripcion = data.get('descripcion');
    const precio = data.get('precio');
    
    const response = fetch('php/dashAgregar.php', {
        method: 'POST',
        body: data
    })
    .then(res => res.text())
    .then(res =>{
        if(res === 'uno'){
            mensajeExito();
            pintarCards();
        }else{
            mensajeError();
        }
    })
}
function mensajeExito(){
    const cardExito = document.createElement('div');
    cardExito.textContent='El producto se subio correctamente';
    cardExito.classList.add('alert', 'alert-success');
    cardAviso.appendChild(cardExito);
    

    setTimeout(function(){
        cardExito.remove(cardExito);
        formDash.reset();
    }, 2000);
}
function mensajeError(){
    const cardError = document.createElement('div');
    cardError.textContent='No se pudo subir producto';
    cardError.classList.add('alert', 'alert-danger');
    cardAviso.appendChild(cardError);
    
    
    setTimeout(function(){
        cardError.remove(cardError);
    }, 2000);
}

