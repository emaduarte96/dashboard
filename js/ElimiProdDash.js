//Variables
const eliminar = document.querySelector('#cajaPrincipal');


//Eventos

Eventos();

function Eventos(){
    eliminar.addEventListener('click', Alerta);
}


//Funciones
function Alerta (e){
    const target = e.target.getAttribute('id');
    let i=0;
    eliminar.innerHTML=``;
    eliminar.innerHTML=` 
    <div id="alerta">
    <div class="cartel p-0 m-0">
        <div class='parrafo pt-2 m-0'>
            <p>¿Que deseas hacer?</p>
        </div>
        <div class='botones p-0 m-0'>
            <button id='volver' type="button" class="btn btn-primary">Volver</button>
            <button id='editar' type="button" class="btn btn-success">Editar</button>
            <button id='eliminar' type="button" class="btn btn-danger">Eliminar</button>
        </div>
    </div>
</div>`;
    eliminar.removeEventListener('click', Alerta);
    
    const btn = document.querySelector('#eliminar');
    btn.addEventListener('click', ()=>{
        i++;
        
        if(i === 1){
            segundaAlerta (target)
        }
    }
        
    ); 
}

function segundaAlerta (b) {
        let i=0;
        eliminar.innerHTML=``;
        eliminar.innerHTML=` 
        <div id="alerta">
        <div class="cartel p-0 m-0">
            <div class='parrafo pt-2 m-0'>
                <p>¿Seguro que deseas eliminar?</p>
            </div>
            <div class='botones p-0 m-0'>
                <button id='volver' type="button" class="btn btn-success">Volver</button>
                <button id='afirmacion' type="button" class="btn btn-danger">Sí, Eliminar</button>
            </div>
        </div>
    </div>`;
    const btn = document.querySelector('#afirmacion');
    btn.addEventListener('click', ()=>{
        i++;
        
        if(i === 1){
            confirmar(b);
        }
    }
        
    ); 
}
function confirmar(b2){
        EliminarProducto(b2);
        fetch('php/dashPintar.php')
        .then(res => res.json())
        .then(data =>{ 
            arreglo = data;
            cajaPrincipal.innerHTML=``;
            for(let i=0; 0 < arreglo.length; i++){
                cajaPrincipal.innerHTML+=`
                <div id="cards" class="p-0 m-0 col-12 col-md-6">
                        <div class="padre">
                            <div id="${data[i].id}" class="hijo">
                                <img id="${data[i].id}" class="p-0 m-0" src="@Resource/${data[i].img}">
                                <h5 id="${data[i].id}">${data[i].nombre}</h5>
                                <p id="${data[i].id}">${data[i].descripcion}</p>
                                <strong><p id="${data[i].id}">$${data[i].precio}</p></strong>
                            </div>
                        </div>
                    </div>
                `
            }
        });
}

function EliminarProducto (p){
    
    var data = new FormData();
    data.append('id', p);
    
    fetch('php/ElimiProdDash.php', {
        method: 'POST',
        body: data
    })
    .then(res => res.text())
    .then(res =>{
        console.log(res);
    }) 
}
