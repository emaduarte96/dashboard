function pintarCards(){
    //Variables
    const cajaPrincipal = document.querySelector('#cajaPrincipal');
    var arreglo=[];

    //Peticion al servidor
    fetch('php/dashPintar.php')
    .then(res => res.json())
    .then(data =>{ 
        arreglo = data;
        cajaPrincipal.innerHTML=``;
        for(let i=0; 0 < arreglo.length; i++){
            cajaPrincipal.innerHTML+=`
            <div id="cards" class="p-0 m-0 col-12 col-md-6">
                    <div class="padre">
                        <div id="${data[i].id}" class="hijo">
                            <img id="${data[i].id}" class="p-0 m-0" src="@Resource/${data[i].img}">
                            <h5 id="${data[i].id}">${data[i].nombre}</h5>
                            <p id="${data[i].id}">${data[i].descripcion}</p>
                            <strong><p id="${data[i].id}">$${data[i].precio}</p></strong>
                        </div>
                    </div>
                </div>
            `
        }
    });
}
pintarCards();
    
