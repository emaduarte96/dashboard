<?php

include_once 'conexion.php';

//Hago consulta a la DB
$result = mysqli_query($conn, "SELECT * FROM producto");


$arrayProducto = [];

 foreach($result as $valor){
    $temp = array(
        "id"=> $valor['id'],
        "nombre"=>$valor['nombre'],
        "descripcion"=> $valor['descripcion'],
        "precio"=>  $valor['precio'],
        "img"=>  $valor['img'], 
      );
      $arrayProducto[] = $temp;
} 


echo json_encode($arrayProducto);

mysqli_free_result($result);

mysqli_close($conn);
?>