<?php
// subirImgAlServer->boolean|str
function subirImgAlServer($name,$maxSize,$rutaDeSubidaDeImg){
  $msg = "";
  $uploadedfileload="true";
  $uploadedfile_size=$_FILES[$name]['size'];

  if ($_FILES[$name]['size']==0){
    $msg=$msg."selecciones una imagen por favor<br><br>";
    $uploadedfileload="false";
  }


  if ($_FILES[$name]['size']>$maxSize){
    $msg=$msg."El archivo es mayor que ".$maxSize.", debes reduzcirlo antes de subirlo<BR>";
    $uploadedfileload="false";
  }

  if (!($_FILES[$name]['type'] =="image/pjpeg" OR $_FILES[$name]['type'] =="image/gif"  OR $_FILES[$name]['type'] =="image/jpeg")){
    $msg=$msg." Tu archivo tiene que ser JPG o GIF. Otros archivos no son permitidos<BR>";
    $uploadedfileload="false";
  }
  $dir_subida = $_SERVER["DOCUMENT_ROOT"].$rutaDeSubidaDeImg;
  $fichero_subido = $dir_subida . basename($_FILES[$name]['name']);

  if($uploadedfileload=="true"){
    if (move_uploaded_file($_FILES[$name]['tmp_name'],$fichero_subido)) {
      return true;
    }else {
        return "no se pudo subir la img";
    }
  }else{
    return $msg;
  }
}
 ?>