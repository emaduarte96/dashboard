DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(80) NOT NULL,
  `precio` int(6) NOT NULL,
  `img` varchar(200) NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;